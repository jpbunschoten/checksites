

describe('Extensions', function () {
    it('should recongize valid ones', function () {
        expect(Extensions.isValid('a.html')).toBeTruthy();
        expect(Extensions.isValid('a.css')).toBeTruthy();
    });
    it('should report true on no extension', function() {
        expect(Extensions.isValid('a')).toBeTruthy();
    });
    it('should report true when last is a slash', function () {
        expect(Extensions.isValid('a.b/')).toBeTruthy();
    });
    it('should recongize invalid ones', function () {
        expect(Extensions.isValid('a.odt')).toBeFalsy();
         expect(Extensions.isNotValid('a.odt')).toBeTruthy();
    });
})