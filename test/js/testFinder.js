
describe('Finder::', function () {
    beforeEach(function () {
        Finder.reset();
        Proxy.async = true
//jasmine.addMatchers(myMatchers);
    });
    it('should find path of a file string', function () {
        expect(Finder.stripFile('a/b/c')).toEqual('a/b');
        expect(Finder.stripFile('a/b/c/d.html')).toEqual('a/b/c');
        expect(Finder.stripFile('d.html')).toEqual('');
    });
    it('should add  with a start file', function () {
        var bron = "v15rhart.helenparkhurst.net"

        Finder.addSite(bron);
        expect(Finder.src[0].bron).toEqual('http://' + bron + '')
        expect(Finder.src[0].uri).toEqual('http://' + bron + '')
        expect(Finder.src[0].stripped.bron).toEqual('/')

    });

    it('should add a /abc.html file', function () {
        var bron = "v15rhart.helenparkhurst.net"

        Finder.addSite(bron);
        expect(Finder.src[0].bron).toEqual('http://' + bron + '/')
        expect(Finder.src[0].uri).toEqual('http://' + bron + '/')
        expect(Finder.src[0].stripped.bron).toEqual('/')

        Finder.add('/abc.html', 'a');
        expect(Finder.src.length).toEqual(2);
        var info = Finder.src[1];
        expect(info.bron).toEqual('http://' + bron + '/');
        expect(info.val).toEqual("http://" + bron + '/abc.html');

    });
    it('should add a XYZ/abc.html file', function () {
        var bron = "xyz.helenparkhurst.net"
        //Finder.bron = 'http://' + bron;
        Finder.addSite(bron);
        expect(Finder.src[0].bron).toEqual('http://' + bron + '/')
        expect(Finder.src[0].stripped.bron).toEqual('/')

        Finder.add('XYZ/abc.html', 'a');
        expect(Finder.src.length).toEqual(2);
        var info = Finder.src[1];
        expect(info.bron).toEqual('http://' + bron + '/');
        expect(info.val).toEqual("http://" + bron + 'XYZ/abc.html');
    });
    it('should register initial site', function () {
        var s = 'qwerty.helenparkhurst.net'
        Finder.addSite(s);
        expect(Finder.user).toEqual('qwerty');
        expect(Finder.src[0].bron).toEqual('http://' + s + '/');
        expect(Finder.src[0].val).toEqual('http://' + s);
    })
    it('should find A\'s in file', function () {
        var t = {'data': '<body><h1> Welcome </h1><a href="hoofdscherm.html" target=x> click here </a> </body> '}

        console.log("t", t);
        console.log("t.data", t.data)
        var $t = $(t.data);
        console.log($t);
        Finder.content = t.data;
        var as = Element.findSrcTagsInText('a', 'href')

        console.log('as', as);
        expect(as.size).not.toEqual(0);
        expect(true).toBeTruthy()
        Finder.findAs();
        console.log("r", Finder.foundTags);
        expect(Finder.foundTags.length).toEqual(1);
    })
    it('should find this a link', function () {
        var txt = '<a href="/hoofdscherm.html">  <h1> Welcome </h1></a> ';
        Finder.content = txt
        console.log($(txt))
        var as = $(txt, function () {
            $.find('qq')
        })
        console.log('AS', as);

        Finder.findAs();
        console.log("r", Finder.foundTags);
        expect(Finder.foundTags.length).toEqual(1);
        expect(true).toBeTruthy();
    })
    it('should find parse riannes hoofdscherm', function () {
        var txt = '<!DOCTYPE html><html><head><title>Main</title><link rel="shortcut icon" type="image/x-icon" href="Favicon.ico"><link rel="stylesheet" type="text/css" href="hoofdscherm.css"><link rel="stylesheet" type="text/css" href="opmaak_menu.css"><meta charset="UTF-8"><style></style></head><body><h1 style= "text-align: center"> Informatica </h1><div class= "menu"><ul><li><a href="index.html"> Start </a> </li><li> <a href="menu.html" target="iframe_main" >Menu</a> </li><li> <a href="contact.html" target="iframe_main">Contact</a> </li><li> <a href="portfolio.html" target="iframe_main" >Portfolio</a></li></ul></div><iframe src="menu.html" name= "iframe_main" width= "400" height="200"></iframe></body></html>';
        Site.site = 'v15rhart.helenparkhurst.net';
        Site.run();
        Finder.content = txt

        var foundTags = Finder.bug().findAs();
        console.log("r", foundTags);
        expect(foundTags.length).toEqual(4);
        expect(true).toBeTruthy();
    })
    it('should find parse jaspers main dir', function () {
        Site.site = 'v15jzwar.helenparkhurst.net';
        Site.run();

        var txt = '<!doctype html><html><head><link rel="shortcut icon" type="image/x-icon" href="favicon.ico"><link rel="stylesheet" type="text/css" href="Opmaak.css"><Meta Charset="UTF-8"><title>Basic information</title></head><body><div id="section"><iframe src="section.html" name="iframe_section" scrolling="yes"></iframe></div><div id="navbar"><a href="section.html" target="iframe_section"><p id="home">Home</p></a><a href="Portofolio/portofolio.html" target="iframe_section"><p>Portofolio</p></a><a href="formulier.html" target="iframe_section"><p>Formulier</p><a/><a href="attributes.html" target="iframe_section"><p>Attributes</p></a><a href="../index.html"/><p>Intro</p></a></div></body></html>'
        Finder.content = txt
        Finder.bron = 'http://' + Site.site + '/main/'

        var foundTags = Finder.bug().findAs();
        console.log("r", foundTags);
        expect(foundTags.length).toEqual(5);
        $.each(foundTags,function(i, t) {
            expect(t.attr).toContain('/main/')
        })
        expect(true).toBeTruthy();
    })
    it('should hanlde # in URI', function() {
        Site.site = 'a.b.c';
        Site.run();
        var s = { 'attr' : Site.home + "" }
        expect(Finder.testHash(s)).toEqual({'element': s.attr, 'status': false})
         var s2 = { 'attr' : Site.home + "#" }
        expect(Finder.testHash(s2)).toEqual({'element': s.attr, 'status': true})
         s2 = { 'attr' : Site.home + "#abc" }
        expect(Finder.testHash(s2)).toEqual({'element': s.attr, 'status': true})
         s2 = { 'attr' : Site.home + "xyz/abc.html#abc" }
        expect(Finder.testHash(s2)).toEqual({'element': s.attr + 'xyz/abc.html', 'status': true})
    })
    describe('Finder: extended # in a', function() {
        var x
        beforeEach(function() {
            Site.site = 'a.b.c'
            Site.run()
            Finder.info = x =  new Info(Site.site + 'abc.html', '', Site.site)
            //console.log(hashRefs);
            x.content = hashRefs;
            x.run();
        })
        it('should find the <a> tags', function() {            
            var lst = Finder.findAs()
            //expect(lst[0].)
            console.log(Site)
            console.log(lst);
        })
    })
});

