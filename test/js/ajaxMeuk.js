//var testPage = '/DynamicHTML5WebProject/targetPage.html',    testFrame;
var testFrame;
var testPage = 'http://v15rhart.helenparkhurst.net';

jQuery(document).ready(function ($) {
    testFrame = $('#testFrame');
    testFrame.one('load', function () {
        
        describe("Verify Test Environment", function () {
            it("should load the target test page.", function () {
                expect(testFrame).toExist();
                expect(testFrame).toBeInDOM();
                expect(testFrame).toHaveAttr('src', testPage);
            });
        });
        testFrame.attr('src', testPage);    // actual load?
    })
})