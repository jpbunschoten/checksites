/*
 
 describe("Asynchronous specs", function () {
 var value;
 
 beforeEach(function (done) {
 setTimeout(function () {
 value = 0;
 done();
 }, 1);
 });
 
 it("should support async execution of test preparation and expectations", function (done) {
 value++;
 expect(value).toBeGreaterThan(0);
 done();
 });
 
 describe("long asynchronous specs", function () {
 beforeEach(function (done) {
 done();
 }, 1000);
 
 it("takes a long time", function (done) {
 setTimeout(function () {
 done();
 }, 9000);
 }, 10000);
 
 afterEach(function (done) {
 done();
 }, 1000);
 });
 
 describe("A spec using done.fail", function () {
 var foo = function (x, callBack1, callBack2) {
 if (x) {
 setTimeout(callBack1, 0);
 } else {
 setTimeout(callBack2, 0);
 }
 };
 
 it("should not call the second callBack", function (done) {
 foo(true,
 done,
 function () {
 done.fail("Second callback has been called");
 }
 );
 });
 });
 });
 


describe("Promises", function () {
    it("is resolved", function (done) {
        var sample_url = 'localhost/checkSites/src/runProxy.php?url=http://v15rhart.helenparkhurst.net'
        var output = "";

        jQuery.getJSON(sample_url)
                .then(function () {
                    output += "JSON got back!";
                }, function (errorData) {
                    // If you want to check the error being displayed, uncomment the debugger
                    // statement and inspect `errorData[2]`.
                    // debugger;
                    expect(output).toEqual("JSON got back!", "** Promise failed **");
                    done();
                }).then(function () {
            expect(output).toEqual("JSON got back!");
            done();
        });

    });
});

 */

describe('proxy returns data', function () {
    var originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;


    afterEach(function () {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });
    beforeEach(function () {
        $.ajaxSetup({
            // async: false, // must be synchronous to guarantee that no tests are run before fixture is loaded
            cache: false,
        });
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000;

    });
    it('should invoke callback', function (done) {
        var lln = 'v15rhart.helenparkhurst.net'
        var uri = 'localhost/checkSites/test/fixtures/t2.html';
        //uri = 'localhost/checkSites/src/runProxy.php?user=v15rhart&url=v15rhart.helenparkhurst.net'
        //uri = lln
        Finder.addSite(lln)
        
        var originalFunction = Finder.findIt;

        var succesFunctionFail = function (data) {
            console.log('oki doki', data)

            setFixtures(data);
            var successCallback = (function (originalFunction) {
                return function (data) {
                    originalFunction(data);
                    done();
                };
            })(successCallback);
        }
        var succesFunction = function (data) {
            //console.log('jfksdcmlkfjd ', data)
            //originalFunction(data);
            Finder.findIt(data);
            done();
        }
      
        Proxy.ajaxCall(0,'v15rhart', uri, uri, succesFunction);
        
        expect(true).toBeTruthy();
        console.log("testing: finder", finder)
        //expect(Finder.src.length).toEqual(2);
    })
});
/*
describe('get file riannes file', function () {
    var uri = 'v15rhart.helenparkhurst.net';
    var huri = 'http://' + uri
    beforeEach(function (done) {
        //var successCallback = Finder.find;
        console.log("yuwbfnmv", testFrame)
        console.log("oitroit", testFrame[0].contentWindow)
        var cw = testFrame.contents();
        console.log(cw);
        testFrame[0].contentWindow.successCallback = (function () {
            console.log('56gb cn ')
            return function (data) {
                Finder.find(data);
                done();
            };
        })(testFrame[0].contentWindow.successCallback);
        //testFrame.contents().find("#btnGetNewMenus").trigger('click');
    });
    /*
     it("should fetch the new restaurant meta data from the server.", function () {
     var listEntry = testFrame.contents().find("#content-area > p").last();
     expect(listEntry).toExist();
     expect(listEntry).toBeInDOM();
     expect(listEntry).toContainElement('a');
     expect(listEntry).toContainText("Robs Bistro");
     expect(listEntry.children('a:first')).toHaveAttr('href', '/restaurant/Robs_Bistro/#title');
     });
     * /
    testFrame.attr('src', uri);
    Finder.addSite(uri)
    $.ajaxSetup({
        async: false, // must be synchronous to guarantee that no tests are run before fixture is loaded
        cache: false,
    });
    // 
    Finder.src = [];
    console.log(finder)
    Finder.findAs();
    expect(Finder.src.length).not.toEqual(0)
    expect(Finder.src[0].val).toEqual('hoof.html')

});
       */