describe('Element from html text', function () {
    beforeEach(function () {
        Finder.info = new Info('','','')
        Site.site = 'x.yy.zzz'
        Site.run();
    })
     var thisHref = 'http://localhost/checkSites'; // $(location).attr('href')
        //Finder.thisHref = thisHref;
        //console.log(Finder.thisHref);
    it('should get the <a>', function () {
        var str = '<body><a href=abc></a></body>';
        Finder.info.content = str;
        var lst = Finder.findAs();
        console.log('lst', lst)
        expect(lst.length).toEqual(1);
        expect(lst[0].attr).toEqual(Site.home + '' +'abc');
    })
    it('should get multiple <a>', function () {
        var str = '<body><a href=abc></a>1<a href=xyz>2</a></body>';
        Finder.info.content = str;
        var lst = Finder.findAs();
        console.log('lst', lst)
        expect(lst.length).toEqual(2);
        expect(lst[0].attr).toEqual(Site.home +'abc');
        expect(lst[1].attr).toEqual(Site.home +'xyz');
    })
    it('should get multiple <a> and iframe', function () {
        var str = '<body><a href=abc></a>1<a href=xyz target=x>2</a><iframe src=qwerty></a></body>';
        Finder.info.content = str;
        var lst = Finder.findAs();
        lst = $.merge(lst, Finder.findIframe())
        console.log('lst', lst)
        expect(lst.length).toEqual(3);
        expect(lst[0].attr).toEqual(Site.home +'abc');
        expect(lst[1].attr).toEqual(Site.home +'xyz');
        expect(lst[2].attr).toEqual(Site.home +'qwerty');
    })
})
describe('find ALL elements from html text', function () {
    beforeEach(function () {
        Finder.info = new Info('','','')
    })
    checkOnTags = function (lst, expected) {
        var allreadySeen = [];
        var cnt = 0;
        $.each(lst, function (i, v) {
            var t1 = $.inArray(v.nodeName, expected) !== -1
            var t2 = $.inArray(v.nodeName, allreadySeen) === -1
            if (t1 && t2) {
                cnt++;
                allreadySeen.push(v.nodeName);
            }
        })
        expect(cnt).toEqual(expected.length);
    };
    it('should get the <a>', function () {
        var str = '<body><a href=abc></a></body>';
        str = str + "<html><head><title></title><src></src></head>" + str + "</html>"
        Finder.info.content = str;
        var lst = Element.findTagsInText();
        console.log('lst', lst);
        expect(lst.length).toEqual(7);
        var expected = ['HTML', 'BODY', "A", 'HEAD', 'TITLE', 'SRC'];
        checkOnTags(lst, expected);
    });
    it('should get multiple <a>', function () {
        var astr = '<a href=abc>abc</a><a href=def>def</a>';
        var str = '<body>' + astr + '</body>';
        str = str + "<html><head><title></title><src></src></head>" + str + "</html>";
        Finder.info.content = str;
        var lst = Element.findTagsInText();
        console.log('lst', lst);
        var expected = ['HTML', 'BODY', "A", 'HEAD', 'TITLE', 'SRC'];
        checkOnTags(lst, expected);

    });
    it('should get multiple <a> and iframe', function () {
        var astr = '<a href=abc>abc</a><a href=def>def</a>';
        var iframeStr = '<iframe src=dfjhs>dfjhs</iframe>';
        var str = '<body>' + astr + iframeStr + '</body>';
        str = str + "<html><head><title></title><src></src></head>" + str + "</html>"
        Finder.info.content = str;
        var lst = Element.findTagsInText();
        console.log('lst', lst)

        var expected = ['HTML', 'BODY', "A", 'HEAD', 'TITLE', 'SRC', 'IFRAME'];
        checkOnTags(lst, expected);
    })
})
describe('find files to tags', function () {
    var strs = [];
    beforeEach(function () {
        var infos = [];
        Site.site = 'a.bb.ccc';
        Site.run();
        Finder.info = new Info('/','','/')
        var str = strs[0] = '<a href=qaz.html></a><a href=wsx.html></a>';
        Finder.info.content = str;
        var lst = Finder.findAs();
        var i = new Info('abc.html', 'a', '/'); // found <a> in / with href = abc.html
        i.content = str; // this href contains 
        i.run();
        infos.push(i);
        infos[0].tags = lst;


        var str = strs[1] = '<a href=edc.html><iframe src=x>x</iframe></a><br><span></span><a href=wsx.html></a>';
        Finder.info.content = str;
        var lst = Finder.findAs();
        console.log('ifrma', Finder.findIframe())
        lst = $.merge(lst, Finder.findIframe())
        i = new Info('def.html', 'iframe', '/xyz.html');
        i.content = str
        i.run();
        infos.push(i);
        infos[1].tags = lst;

        i = new Info('def.html', 'a', '/xyz.html');
        i.content = str
        i.run();
        infos.push(i);
        infos[2].tags = lst;

        Finder.src = infos

    })
    it ('should contain proper info', function() {
        expect(Finder.src[0].grootte).toEqual(strs[0].length)
        expect(Finder.src[1].grootte).toEqual(strs[1].length)
    })
    it('should find the files attached to the a tag', function () {
        var lst = Element.filesFromSrcTag('a');
        //console.log(lst);
        $.each(['abc.html', 'def.html'], function (i, v) {
            console.log(v,lst,$.containsInArray(v, lst) )
            var t = $.containsInArray(v, lst) 
            expect(t).toBeTruthy('file :"' + v + '" should be in list');
        })
      })
    it('should NOT find the files attached to the h1 tag', function () {
    
       var  lst = Element.filesFromSrcTag('h1');
        //console.log(lst);
        $.each(['abc.html', 'def.html'], function (i, v) {
            var t = $.containsInArray(v, lst) 
            expect(t).toBeFalsy('file :"' + v + '" should not be in list');
        })
          })
    it('should find the files attached to the iframe tag', function () {

        var lst = Element.filesFromSrcTag('iframe');
         console.log(lst);
        $.each([ 'def.html'], function (i, v) {
            var t = $.containsInArray(v, lst)
            expect(t).toBeTruthy('file :' + v + ' should be in list');
        })
    })
   
})

describe('find files from tagNames', function () {
    var strs = []
    beforeEach(function () {
        
        var infos = [];
        Site.site = 'a.bb.ccc';
        Site.run();
        Finder.info = new Info('/','','/')
        var str = strs[0] = '<a href=qaz.html></a><a href=wsx.html></a>';
        Finder.info.content = str;
        var lst = Element.findTagsInText();
        var i = new Info('abc.html', 'a', '/'); // found <a> in / with href = abc.html
        i.content = str; // this href contains 
        i.run();
        infos.push(i);
        infos[0].tags = lst;


        var str = strs[1] = '<a href=edc.html><iframe src=x>x</iframe></a><br><span></span><a href=wsx.html></a>';
        Finder.info.content = str;
        lst = Element.findTagsInText();        
        i = new Info('def.html', 'iframe', '/xyz.html');
        i.content = str
        i.run();
        infos.push(i);
        infos[1].tags = lst;

lst = Element.findTagsInText();        
        i = new Info('def.html', 'a', '/xyz.html');
        i.content = str
        i.run();
        infos.push(i);
        infos[2].tags = lst;

        Finder.src = infos          

    });
    it('should find the files attached to a specific tag', function () {
        var lst = Element.filesFromTagNames(['a', 'src']);
        $.each(['/', '/xyz.html'], function (i, v) {
            var t = $.inArray(v, lst) !== -1
            expect(t).toBeTruthy();
        })
        var lst = Element.filesFromTagNames(['h1']);
        console.log(Finder.src, lst)
        $.each(['/xyz.html'], function (i, v) {
            var t = $.inArray(v, lst) !== -1
            expect(t).toBeTruthy(v);
        })
    })
})

describe('find All attributes', function () {
    beforeEach(function () {
        var infos = [];

        var str = '<a href=qaz.html><br><src class=x style="dsfsad" ></src></a><a href=wsx.html></a>';
        Finder.content = str;
        var lst = Element.findTagsInText();
        infos.push(new info('abc.html', 'a', '/'))
        infos[0].tags = lst;


        var str = '<a href=edc.html class="a b"></a><a href=wsx.html target="e"><h1 id=2></h1></a>';
        Finder.content = str;
        var lst = Element.findTagsInText();
        infos.push(new info('def.html', 'a', '/xyz.html'))
        infos[1].tags = lst;

        Finder.src = infos

    })
    it('should find the files attached to a specific tag', function () {
        //console.log(Finder.src);
        var lst = Element.allAttributes(Finder.src[0].tags);
        $.each(['href', 'class', 'style'], function (i, v) {
            var t = $.inArray(v, lst) !== -1
            expect(t).toBeTruthy(v);
        })
        //console.log(Finder.src[0], lst)

        var lst = Element.allAttributes(Finder.src[1].tags);
        $.each(['href', 'class', 'target', 'id'], function (i, v) {
            var t = $.inArray(v, lst) !== -1
            expect(t).toBeTruthy(v);
        })
        //console.log(Finder.src[1], lst)
    })
})

describe('to tagnames', function () {
    beforeEach(function () {
        var infos = [];

        var str = '<a href=qaz.html><br><src class=x style="dsfsad" ></src></a><a href=wsx.html></a>';
        Finder.content = str;
        var lst = Element.findTagsInText();
        infos.push(new info('abc.html', 'a', '/'))
        infos[0].tags = lst;


        var str = '<a href=edc.html class="a b"></a><a href=wsx.html target="e"><h1 id=2></h1></a>';
        Finder.content = str;
        var lst = Element.findTagsInText();
        infos.push(new info('def.html', 'a', '/xyz.html'))
        infos[1].tags = lst;

        Finder.src = infos

    })
    it('build a tagNames array', function () {
        //console.log(Finder.src);
        var lst = Element.toTagNames(Finder.src[0].tags);
        console.log(Finder.src[0], lst)
        $.each(['a', 'br', 'src'], function (i, v) {
            var t = $.inArray(v, lst) !== -1
            expect(t).toBeTruthy(v);
        })

        var lst = Element.toTagNames(Finder.src[1].tags);
        console.log(Finder.src[1], lst)
        $.each(['a', 'h1'], function (i, v) {
            var t = $.inArray(v, lst) !== -1
            expect(t).toBeTruthy(v);
        })
    })
})

