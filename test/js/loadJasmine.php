<?php

$jasmineVersion = "2.3.4";
$base = "/lib/jasmine";
$jBase = "$base/jasmine-$jasmineVersion";


//  <script type="text/javascript"  src="$base/mock-ajax.js"></script

print <<<EOI

<link rel="shortcut icon" type="image/png" href="$jBase/lib/jasmine-$jasmineVersion/jasmine_favicon.png">
<link rel="stylesheet" href="$jBase/lib/jasmine-$jasmineVersion/jasmine.css">

<script type="text/javascript" src="$jBase/lib/jasmine-$jasmineVersion/jasmine.js"></script>
<script type="text/javascript" src="$jBase/lib/jasmine-$jasmineVersion/jasmine-html.js"></script>
<script type="text/javascript" src="$jBase/lib/jasmine-$jasmineVersion/boot.js"></script>
        
       
<script type="text/javascript" src="$base/jasmine-jquery.js"></script>       
        
<script type="text/javascript" src="$base/js/testHelpers.js"></script>
<script type="text/javascript" src="$base/js/toBeTypeOf.js"></script>

EOI;
