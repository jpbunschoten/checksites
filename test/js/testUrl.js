
describe('Url', function () {
    beforeEach(function () {
        //jasmine.addMatchers(myMatchers);
        Finder.info = new Info('','','')
    });
    describe('stripSlashes', function () {
        it('should remove "//"', function () {
            var s = Url.stripSlashes("abc//def//ghi/")
            expect(s).toEqual("abc/def/ghi/")
        })
        it('should leave "://"', function () {
            var s = Url.stripSlashes("abc://def//ghi/")
            expect(s).toEqual("abc://def/ghi/")
        })
    })
    describe('must remove # ', function() {
        beforeEach(function() {
            Site.site = 'a.b.c';
            Site.run();
            
        })
        it('should remove plain #', function() {
            var uri = Site.home + '#'
            expect(Url.stripHash(uri)).toEqual(Site.home)
        })
        it('should remove plain #bliblablo', function() {
            var uri = Site.home + '#bliblablo'
            expect(Url.stripHash(uri)).toEqual(Site.home)
        })
        it('should remove plain abc.html#bliblablo', function() {
            var uri = Site.home + 'abc.html#bliblablo'
            expect(Url.stripHash(uri)).toEqual(Site.home + 'abc.html')
        })
        it('should remove plain xyz/abc.html#bliblablo', function() {
            var uri = Site.home + 'xyz/abc.html#bliblablo'
            expect(Url.stripHash(uri)).toEqual(Site.home + 'xyz/abc.html')
        })
    })
    describe('strippedHome', function () {
        beforeEach(function() {
            Site.site = 'a.b.c'
            Site.run();
            console.log(Site)
        })
        it('should remove first part of uri', function () {
            //Finder.info.HP = '.b.c';
            var uri = 'http://a.b.c'
            expect(Url.stripHome(uri)).toEqual('')
        })
        it('should leave after fist part', function () {
            //Finder.info.HP = '.b.c';
            var uri = 'http://a.b.c/D/E'
            expect(Url.stripHome(uri)).toEqual('/D/E')
        })
    })
    describe('isOwner', function () {
        it('home and http', function () {
            Finder.info.home = "http://abc.def";
            expect(Url.isOwner("abc.def")).toBeTruthy('missing http');
            expect(Url.isOwner("http://abc.def")).toBeTruthy('both same');
        })
        it('home and no http ', function () {
            Finder.info.home = "abc.def";
            expect(Url.isOwner("abc.def")).toBeTruthy('no http');
            expect(Url.isOwner("http://abc.def")).toBeTruthy('uri has http');
        })
        it('home and homeWithExtension', function () {
            Finder.info.home = "http://a.b.c/"
            expect(Url.isOwner(Finder.info.home + "xyz/qwert.html")).toBeTruthy()
            expect(Url.isNotOwner(Finder.info.home + "xyz/qwert.html")).toBeFalsy()
        })
        it('should detect this', function () {
            Finder.info.home = "http://v15rhart.helenparkhurst.net/";
            expect(Url.isOwner("http://v15rhart.helenparkhurst.net/hoofdscherm.html")).toBeTruthy()
        })
    });
    describe('absolute', function () {
        it('should act on xxx.HP', function () {
            var s = 'v15abc.helenparkhurst.net'
            var hs = 'http://' + s
            var hss = hs + '/'
            Site.site = s;
            Site.run();
            expect(Url.toAbsolute(s)).toEqual(hs, 's')
            expect(Url.toAbsolute(hs)).toEqual(hs, 'hs')
            expect(Url.toAbsolute(hss)).toEqual(hss, 'hss')
            expect(Url.toAbsolute('/')).toEqual(hss, '/')
        })
    })
    describe('general', function () {
        afterEach(function () {
            Url.debug = false;
        })
        it('should recognize relative paths', function () {
            Site.site = "abc.hp.net";
            Site.run();
            console.log(Site);
            Finder.info.uri = Site.home
            //Finder.url = "v.html"
            expect(Url.isRelative("v.html")).toBeTruthy('v.html is relative');
            expect(Url.isRelative("/v.html")).toBeFalsy('/v.html is absolute (in ROOT)');           
            expect(Url.isRelative(Site.home)).toBeFalsy('home:'+Site.home+':should be absolute');
        });
        it('should recognize absolute paths', function () {
            Site.site = "abc.hp.net";
            Site.run();
            expect(Url.isAbsolute("/v.thml")).toBeTruthy();
            expect(Url.isAbsolute("http::/BliBlaBlo")).toBeTruthy();
        });

        it('should convert to http url', function () {
            var uri = 'vavc.helenparkhurst.net'
            Site.site = uri;
            Site.run();
            expect(Url.toAbsolute(uri)).toEqual('http://' + uri);
            var uri = 'bliblablo'
            expect(Url.toAbsolute(uri)).toEqual('http://' +Site.site + '/' + uri);
        })
        it('should return base', function () {
            var uri = 'abc.helenparkhurst.net'
            uriHttp = 'http://' + uri;
            expect(Url.getBase(uriHttp)).toEqual(uri);
        })
    })
    describe('to single Http', function () {
        it('should work', function () {
            var http = "http://"
            var uri = http + "a.b.c"
            expect(Url.toSingleHttp(http + uri)).toEqual(uri)
            expect(Url.toSingleHttp(http + http + uri)).toEqual(uri)
            expect(Url.toSingleHttp(http + http + uri)).toEqual(uri)
        })
    })
    describe('toAbsolute', function () {
        var tst
        var uri = 'v15rhart.helenparkhurst.net'
        beforeEach(function() {
             Site.site = uri;
            Site.run();
        })   
        afterEach(function() {
            Url.debug = false;
            
        })
        it('should extend uri with http', function() {
            var x = Url.toAbsolute(uri);
            expect(x).toEqual('http://'+uri)
        })
        $.each(['/a', '/'], function (i, v) {
            describe(" testing on '" + v + "'", function () {
                beforeEach(function () {
                    Finder.info.home = 'http://' + uri;
                })
                it("should convert File to absolute " + v, function () {
                    Finder.info.home = uriHttp = 'http://' + uri;
                    var tst = v
                    expect(Url.toAbsolute(tst)).toEqual(uriHttp + tst);
                })
            })
        })

        it("testing on '' ", function () {
            Finder.info.home = uriHttp = 'http://' + uri;
            var tst = uri
            expect(Url.toAbsolute(tst)).toEqual("http://" + tst);
        })
        it("should convert uri to absolute ", function () {
            Finder.info.home = uriHttp = 'http://' + uri;
            var tst = uri
            expect(Url.toAbsolute(tst)).toEqual("http://" + tst);
        })
        it("should convert File 'abc.html' to absolute ", function () {
            Finder.info.home = uriHttp = 'http://' + uri;
            var tst = 'abc.html'
            expect(Url.toAbsolute(tst)).toEqual("http://" + uri + "/" + tst);
        })
        it("should leave bron unchanged ", function () {
            var tst = 'abc.html'
            var home = Site.home
            expect(Url.toAbsolute(tst)).toEqual(Site.home   + tst);
            expect(Site.home).toEqual(home);
        })
    })
    describe('contains site', function () {
        it('first test', function () {
            var uri = 'a.helenparkhurst.net'
            Site.home = uri
            Site.HP = '.helenparkhurst.net'
            expect(Url.containsSite(uri)).toBeTruthy('uri contains site')
            Site.HP = 'a.b.c';
            expect(Url.containsSite('http://' + Site.HP + '/xyz/qwert.html')).toBeTruthy()
             Site.HP = 'a.b.c';
            expect(Url.containsSite('http://b.c/xyz/qwert.html')).toBeFalsy()
        })
    })
describe('strippedDoubleColon', function () {
        it('should leave  undisturbed', function () {            
            var uri = 'http://a.b.c/abc/./x'
            expect(Url.stripDotDot(uri)).toEqual('http://a.b.c/abc/./x')
        })
        it('remove single ..', function () {            
            var uri = 'http://a.b.c/abc/../x'
            expect(Url.stripDotDot(uri)).toEqual('http://a.b.c/x')
        })
        it('remove single ..', function () {            
            var uri = 'http://a.b.c/abc/def/../../x'
            expect(Url.stripDotDot(uri)).toEqual('http://a.b.c/x')
        })
        
    })
});
