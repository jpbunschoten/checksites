
describe('tests with fixture', function () {
    beforeEach(function () {
        Finder.reset();
        Finder.home = 'abc.xyz.qwert'
        Finder.url = 'http://'
//jasmine.addMatchers(myMatchers);
    });


    describe('get file from proxy.php and parse', function () {
        it('', function () {
            var uri = '../../checkSites/test/fixtures/' + 't1.html'
            $.ajaxSetup({
                async: false, // must be synchronous to guarantee that no tests are run before fixture is loaded
                cache: false,
            });
            Proxy.ajaxCall('abc', uri, uri, Finder.find)
            Finder.src = [];
            console.log(Finder.data)
            Finder.findAs();
            expect(Finder.src.length).not.toEqual(0)
        })
    })
    describe('find all tags in document.data', function () {
        var o;
        beforeEach(function (done) {
            var lln = 'v15rhart.helenparkhurst.net'
            var uri = 'localhost/checkSites/test/fixtures/t2.html';
            uri = lln

            Finder.addSite(lln)
            o = new info(uri, '', uri)
            var succesFunction = function (data) {
                Finder.content = data.data;
                done();
            }

            Proxy.ajaxCall('v15rhart', uri, uri, succesFunction);
        })
        it('should get from root', function () {
            Add.findTags(o);
            expect(true).toBeTruthy();
            expect(o.tags).toContain('a')
            console.log("testing: finder", o, Finder.tags)
        })
        it('should get from root', function () {
            Add.findTags(o);
            Add.findAttributes(o);
            console.log(o);
            expect(true).toBeTruthy();
            expect(o.tags).toContain('a')
            console.log("testing: finder", o, Finder.tags)
        })
    })
    
})


