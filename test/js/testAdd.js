
describe('testing add', function () {
    var x = {attr: ''}
    var bron;
    var home;
    beforeEach(function () {
        x.attr = 'abc.html';
        bron = 'qwerty.html';
        home = 'zaqwsx.rfv'
        Finder.HP = home
    });
    afterEach(function () {
        x.attr = ''
        Finder.src = []
        Add.debug = false;
    })
    it('first add', function () {
        Finder.home = Url.toHttp(home);
        Finder.bron = Url.toAbsolute(bron)
        expect(Finder.bron).toEqual('http://' + home + '/' + bron, 'finder bron must be full')
        Finder.strippedBron = Url.stripHome(Finder.bron);
        expect(Finder.strippedBron).toEqual('/' + bron, 'bron should have been stripped')
        Add.add(x, 'x');
        //console.log(Finder.src)
        expect(Finder.src.length).toEqual(1);
    });
    it('only 1 registration', function () {
        Finder.home = Url.toHttp(home);
        Finder.bron = Url.toAbsolute(bron)
        expect(Finder.bron).toEqual('http://' + home + '/' + bron, 'finder bron must be full')
        Finder.strippedBron = Url.stripHome(Finder.bron);
        Finder.bronUri = Url.stripBron(Finder.bron);
        expect(Finder.strippedBron).toEqual('/' + bron, 'bron should have been stripped')
        expect(Finder.bronUri).toEqual('/', 'stripped URI of bron')
        // console.log(x);
        expect(x.attr).toEqual('abc.html')
        Add.add(x, 'x');
        expect(Finder.src.length).toEqual(1, 'first add');
        Add.add(x, 'x');
        console.log(Finder.src)
        expect(Finder.src.length).toEqual(1, ' same 1st add');
    });
    it('should add only when not present', function () {
        Finder.home = Url.toHttp(home);
        Finder.bron = Url.toAbsolute(bron)
        expect(Finder.bron).toEqual('http://' + home + '/' + bron, 'finder bron must be full path')
        Finder.strippedBron = Url.stripHome(Finder.bron);
        Finder.bronUri = Url.stripBron(Finder.bron);
        expect(Finder.strippedBron).toEqual('/' + bron, 'bron should have been stripped')
        // console.log(x);
        expect(x.attr).toEqual('abc.html', 'setup check')
        Add.add(x, 'x');
        expect(Finder.src.length).toEqual(1, 'first add');
        expect(Finder.src[0].uri).toEqual(Finder.home + '/' + x.attr)
        Add.add(x, 'x');
        console.log(Finder.src)
        expect(Finder.src.length).toEqual(1, ' same 1st add');


        x.attr = 'a.b.c'
        Finder.bron = '/a/b/d.html';
        Finder.strippedBron = Url.stripHome(Url.toAbsolute(Finder.bron))
        Finder.bronUri = Url.stripBron(Finder.bron);
        Finder.add(x, 'x');
        expect(Finder.src.length).toEqual(2, "second add");
        Finder.add(x, 'x');
        expect(Finder.src.length).toEqual(2, 'same second');
        Finder.bron = 'a.b.c/path.html';
        Finder.strippedBron = Url.stripHome(Url.toAbsolute(Finder.bron))
        Finder.add(x, 'x');
        expect(Finder.src.length).toEqual(3);
        Finder.add(x, 'x');
        expect(Finder.src.length).toEqual(3, 'same 3rd');
    });
})

