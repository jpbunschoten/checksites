<?php

class ProxyTest extends PHPUnit_Framework_TestCase {

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    function testAlive() {
        $this->assertTrue(true);
    }

    function testRetrieval() {

        $url = 'checkSites/test/fixtures/t1.html';
        $url = 'v15rhart.helenparkhurst.net';
        $user = 'v15abc';
        $bron = '/checkSites';
        
       
        $p = new Proxy();
        $p->url = $url;
        $p->user = $user;
        $p->bron = $bron;
        $jso = $p->run()->jsonObject;
        print_r($jso);
        $r = json_decode($jso);
        $this->assertEquals($r->bron, $bron);
        $this->assertEquals($r->user, $user);
        $this->assertEquals($r->url, $url);
    }

}
