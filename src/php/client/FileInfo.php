<?php

class FileInfo {

    function __construct($f) {   // $f obtained via FILE()
        $this->nr_lines = count($f);
        $c = 0;
        if (is_array($f)) {
            foreach ($f as $line) {
                $c += strlen($line);
            }
        }
        $this->nr_chars = $c;
    }

}
