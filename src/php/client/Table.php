<?php

class Table {

    public $rows = array();
    public $tds = array();
    public $ths = array();
    public $labels = array();
    public $tbody;
    public $thead;
    public $msg;

    public function reset() {
        $this->rows = array();
        $this->tds = array();
        $this->ths = array();
        $this->labels = array();
        return $this;
    }

    private function tx($k, $in) {
        return "<$k $in>$this->msg</$k>";
    }

    function tr($in = "") {
        $this->rows[] = $this->tx("tr", $in);        
        return $this;
    }

    function trs() {
        $this->msg = implode(' ', $this->rows);
        $this->rows = array();
        return $this;
    }

    private function ts() {
        //print_r(func_get_args());
        return implode(' ', func_get_args());
    }

    function td($in = "") {
        $this->tds[] = $this->tx("td", $in);
        return $this;
    }

    function tds() {
        $this->msg = implode(' ', $this->tds);
        $this->tds = array();
        return $this;
    }

    function th($in = "") {
        $this->ths[] = $this->tx("th", $in);
        return $this;
    }

    public function ths() {
        $this->msg = implode(' ', $this->ths);
        $this->ths = array();
        return $this;
    }

    function table($in = "border=1") {
        //$this->ths()->msg;
        //$ths = $this->thead()->thead;
        //$this->tds()->msg;
        //$tds = $this->tbody()->tbody;      
        $ths = $this->thead;
        $tds = $this->tbody;
        $this->msg = implode(' ', array($ths, $tds));
        $s = $this->tx("table", $in);
        $this->reset();
        return $s;
    }

    function tbody($in = '') {
        $this->tbody = $this->tx('tbody', $in);
        $this->td = array();
        return $this;
    }

    function thead($in = '') {
        $this->thead = $this->tx('thead', $in);
        $this->th = array();
        return $this;
    }

    function label($id) {
        $this->labels[] = $this->tx('label', "for='$id' ");
        return $this;
    }

    function labels() {
        $this->msg = implode(' ', $this->labels);
        $this->labels = array();
        return $this;
    }

    function radio($id, $name, $value, $checked, $v = "") {
        if ($v == "")
            $v = $value;
        $this->msg = "<input type=radio id=$id name=$name value='$value' $checked>$v";
        return $this;
    }
    function el($s) {
        $this->msg = $s;
        return $this;
    }

}
