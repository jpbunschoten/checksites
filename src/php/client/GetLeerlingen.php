<?php

class GetLeerlingen {

    public $x = array();
    public $docenten = array();
    public $info = array();

    function __construct($klas) {

        $f = FILE(Klassen::DATADIR . $klas . ".txt");
       
        sort($f);

        foreach ($f as $lln) {
             
            $lln = trim($lln);
            if (strpos("$lln", "#") === 0)
                continue;
            $info = explode(' ', strtolower($lln));
            
            if (count($info) > 1) {
                $this->docenten[] = $info[1];
            }
            $this->x[] = $info[0];
        }
        return $this;
    }

    function get() {
        return $this->x;
    }

    function getDocenten() {
        return $this->docenten;
    }

}
