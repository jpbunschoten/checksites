<?php

function extension($file, $ext) {
    return preg_match('/^0.*[.]' . $ext . '$/', $file);
}

function get_klassen($fs) {
    foreach ($fs as $ks) {
        foreach (glob("./{$ks}*") as $k) {
            $x[] = $k;
        }
    }
    asort($x);
    return $x;
}

function get_klas($k) {
    $d = dir("./$k");
    $x = Array();
    while (false !== ($entry = $d->read())) {
        if (is_file($entry) && (extension($entry, "txt") || $entry == "jacob.txt")) {
            $x[] = substr($entry, 0, strlen($entry) - strlen(".txt"));
        }
    }
    $d->close();

    return $x;
}
?>
<table>
    <?php
    //foreach(get_klassen(array('094v', '10', '11')) as $k) {
    //foreach(get_klassen(array('104v', '11')) as $k) {
    foreach (get_klassen(array('114v')) as $k) {
        $fl = FILE($k);
        $x = Array();
        foreach ($fl as $f) {
            if ($f[0] == '#')
                continue;
            $x[] = $f;
            //print "<tr><td>$k</td><td><a href=http://$f target=_blank>$f</a></td></tr>";
        }
        sort($x);
        foreach ($x as $f) {
            print "<tr><td>$k</td><td><a href=http://$f target=_blank>$f</a></td></tr>";
        }
    }
    ?>
</table>
