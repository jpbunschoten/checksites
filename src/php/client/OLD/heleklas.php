<?php

// met deze interface worden de websites van alle leerlingen uit de 4e klas gevalideerd.
include_once "klassen.php";
require_once "./paths/getlinks.php";
$P = "./site_validate";
include_once "$P/output.php";
include_once "$P/validate_css.php";

$klassen = (new Klassen())->get();
foreach ($klassen as $klas) {
    $f = (new GetLeerlingen($klas))->get();
    foreach ($f as $lln) {
        //$lln = trim($lln);
        //if (strpos("$lln", "#") === 0) continue;
        //$t = microtime(true);
        $search_links = new SearchLinks($lln);
        //$search_links->Search();
        print "processing $lln<br>";
        print "Found " . $search_links->cnt() . " Links<br>";
        
        //print "searching took about " . (microtime(true)-$t)/1 . " msec";
        //include_once "$P/output.php";
        //$search_links->show();		flush();

        $o = new validate_css($search_links->links);
        $o->table();
        print "<br><br>";
        $search_links->__destruct();
        //$o->__destruct();
        unset($search_links);
        unset($o);
    }
}