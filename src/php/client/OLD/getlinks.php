<?php

function debugparser() {
    global $debug;
    if (isset($debug))
        return $debug;
    return false;
}

function pre($s = "") {
    if (debugparser())
        print "<{$s}pre>";
}

function nl($h = false) {
    if (debugparser())
        print "\n";
}

function br() {
    print "<br>";
}

function pr($s) {
    if (debugparser()) {
        pre();
        print htmlspecialchars($s);
        pre('/');
    }
}

function pro($o) {
    if (debugparser()) {
        pre();
        print_r($o);
        pre('/');
    }
}

class HtmlTokens {

    public function __construct($source) {
        $src = preg_replace("/<!--(.|\s)*?-->/", "", $source);  // strip comment
        $re = '@<([^>]+)>@iU';         // all tags 
        preg_match_all($re, $src, $arr);

        $re = '@(src|href|action)\w*=@iU';
        $ar = array();
        foreach ($arr[1] as $k => $a) {
            $t = array();
            preg_match_all($re, $a, $t);
            if (count($t[0]))
                $ar[] = $a;
        }
        $src = implode(" ", $ar);

        $this->toks = $t = new Tokens($src);
        $t->StripWhiteSpace();
    }

}

// class

class ALink {

    function __construct($url, $daddy) {
        $this->url = $url;
        $this->daddy = $daddy;
    }

}

class GetLinks {

    var $urls = array();
    var $visited = array();
    var $k = 0;
    var $allowed_extensions = array("HTML", "htm", "html", "php", "shtml");

    function __construct($url) {

        $this->GetServerName($url);
        pr(__FUNCTION__ . $this->server_url);
//    $this->Links[] = new ALink($url, "");
        $this->urls[0] = $url;
        $this->parent[0] = "";
        $this->k = 0;
        $this->process($url, "");      // is recursive
        $this->SortAndUniqueUrls();
    }

    function home() { //returns home of this site (in HTML format)
;
    }

    function GetExtension($s) {
        $p1 = strrpos($s, "/");
        if ($p1 && $p1 == strlen($s) - 1)
            return "";
        $p2 = strrpos($s, ".");
        if ($p2 === false)
            return "";
        if ($p2 < $p1)
            return "";
        return substr($s, $p2 + 1);
    }

    function AllowedExtension($e) {
        if ("$e" === "")
            return true;
        return (in_array($e, $this->allowed_extensions));
    }

    function x() {
        $k = $this->k;
        $u = $this->urls[$k];
        $ps = $this->parent[$k];
        foreach ($ps as $p) {
            print "<br>\$k is now $k; $p ==> $u";
        }
    }

    function BuildBase($lnk) {             // full URL, strip last (must be a file?)
        pr(__FUNCTION__ . " building base for $lnk");
        $pslash = strrpos($lnk, '/'); // pointer to last slash
        $railroadcross = strpos($lnk, '#'); // first occurence of '#'
        if ($railroadcross == 1)
            return "";    // i.e. href=#
        $pdot = strrpos($lnk, '.');
        $pfirstslash = strpos($lnk, '/');
        if ($pfirstslash == 0) {
            return "";   // absolute reference, href=//
        }
        if ($pdot > $pslash) {  // assuming file with extension
            if ($pslash == false)
                return "";
            $lnk = substr($lnk, 0, $pslash);
        }
//pr(__FUNCTION__. $this->server_url);
        $s = substr($lnk, strlen($this->server_url) + strlen("http://")) . "";
        pr(__FUNCTION__ . " lnk: $s");
        return $s;
    }

    function CanonizeUrl($url) {
        $p = 0;          // start pos
        if (strpos($url, "http://") == 0) { // starts with http
            $p = 7;
            $url = substr($url, $p);
        }
        while (strpos($url, "//")) {
//pr(__CLASS__."::".__FUNCTION__." ".$url);		
            $url = preg_replace('@//@', "/", $url);
        }
        if ($p == 7)
            $url = "http://" . $url;
        pr(__FUNCTION__ . " canon: $url");
        return $url;
    }

    function process($url, $base, $lvl = 0) {
        $u = $url;
        if ($lvl > 10)
            return;
        if (in_array($u, $this->visited)) {
            pr(__FUNCTION__ . " $u already present");
            return;
        }
//		set_time_limit(30);
        pr(__FUNCTION__ . " url:" . $u);
        pr("base:" . $base);
        pr("urls ");
        pro($this->urls);
        pr("visited ");
        pro($this->visited);
        flush();
        $this->visited[] = $u;

        $f = @file_get_contents($u);
        pr(__FUNCTION__ . " starting parser {$this->k}");
        $p = new TheParser($f, $this->k);
        $f = "";
        pr(__FUNCTION__ . " parser object for '$u'");
        pro($p);
        foreach ($p->results() as $lnk) {
            if ($this->Valid($lnk)) {
                $Build = $this->BuildLink($lnk, $base);
                $TBuild = $this->buildLink(rawurlencode($lnk), $base);
                if (in_array($Build, $this->urls))
                    continue;
                if (in_array($Build, $this->visited))
                    continue;

                if (!$this->page_exists("$Build")) {
                    $this->visited[] = $Build;
                    print "<a href='$Build'><b>$Build</b></a> is not alive?";
                    $Tgt = "target='_blank'";
                    print "<a $Tgt href='http://jigsaw.w3.org/css-validator/validator?uri=$Build'>CSS </a>";
                    print "<a $Tgt href='http://validator.w3.org/check?uri=$Build&ss=1&doctype=HTML5'>HTML </a>";
                    print "<br>";
                    flush();
                    continue;
                }
                $this->k++;
                $this->urls[$this->k] = "$Build";
                $this->parent[$this->k] = $u;
//$this->x();
//print "<br>{$this->k}:$u ==> $lnk ". count($p->results());

                pr(__FUNCTION__ . " : processing (recursive) for '$Build'");
                $this->process($Build, $this->buildBase($Build), $lvl + 1);   // recursion	
                pr(__FUNCTION__ . " out of recursion on '$Build'");
            } else {
                pr(__FUNCTION__ . " skipping lnk $lnk");
                ;
            }
        }
    }

    function page_exists($p) {
//print "tesing page_exists for $p";
        $t = false;
        if ($fp = @fopen("$p", 'r')) {
            $t = true;
            fclose($fp);
        }
        /*
          else {
          print "attempt to read file '$p' (fopen failed)<br>"; flush();
          $ar = array(); $ar = FILE($p);$t = (count($ar) > 0); unset($ar);
          }
         */
        return $t;
    }

    function SortAndUniqueUrls() {
        return;
        $this->urls = array_unique($this->urls); // wow that was easy
        foreach ($this->urls as $k => $v) {
            $t[$k] = $this->parent[$k];
        }
        $this->parent = $t;
    }

    function GetServerName(& $url) {
        if (substr_compare($url, "http:", 0, 5) != 0) // so missing
            $url = "http://" . $url;
        $re = "@^(http://)?([^/]+)@i";
        preg_match_all("$re", $url, $matches, PREG_SET_ORDER);

        $this->server_url = $matches[0][2] . "/";
        $this->plain_server = $this->server_url;
        $p = strpos($this->plain_server, "www.");
        if ($p !== false) {
            $a = substr($this->plain_server, 0, $p);
            $b = substr($this->plain_server, $p + 4);
            $this->plain_server = $a . $b;
//			print $this->plain_server;
        }
//		print $p; print $this->server_url;
    }

    function Valid($lnk) {
        pr(__FUNCTION__ . " $lnk");
        if ($lnk == "mailto:")
            return false;
        if (strpos($lnk, '#') !== false)
            return false;
        if (in_array($lnk, $this->visited))
            return false;
        if (in_array($lnk, $this->urls))
            return false;
        $e = $this->GetExtension($lnk);
        if (!$this->AllowedExtension($e))
            return false;
        if (strpos($lnk, '/') == 0)
            return true; // local to base
        if (preg_match("%{$this->plain_server}%i", $lnk))
            return true;
        if (preg_match("%{$this->server_url}%i", $lnk))
            return true;
        return false;
    }

    function BuildLink($lnk, $base) {
        pr(__FUNCTION__ . " lnk $lnk & base '$base'"); // $x = strpos($lnk, "http://"); print_r($x);

        if (substr($lnk, 0, strlen("http://")) === "http://")
            return $lnk;
        return $this->CanonizeUrl("http://" . $this->server_url . "/" . $base . "/" . $lnk);
    }

    function links() {
        return $this->urls;
    }

}

class SearchLinks extends GetLinks {

    function __construct($base) {
        parent::__construct($base);
        $this->links = $this->urls;
    }

    function Search() {
        ;
    }

    function links() {
        return $this->urls;
    }

    function show() {
        print "<table border=1>";
        foreach ($this->urls as $k => $l) {
            $x = "<td><a href={$this->parent[$k]} target=_blank>{$this->parent[$k]}</a></td>";
//$x = "";
            print "<tr>$x<td> $l</td></tr>";
        }
        print "</table>";
    }

}

?>