<?php


class ValidateHTML extends output {

    //class validate_html  {

    function __construct($list) {
        parent::__construct();
        $this->list = $list;
        //		$this->check();
    }

    function getRemoteFile($url) { // 
// see http://stackoverflow.com/questions/10617178/why-can-i-not-download-files-from-some-sites-like-this
        // get the host name and url path
        $parsedUrl = parse_url($url);
        $host = $parsedUrl['host'];
        if (isset($parsedUrl['path'])) {
            $path = $parsedUrl['path'];
        } else {
            // the url is pointing to the host like http://www.mysite.com
            $path = '/';
        }

        if (isset($parsedUrl['query'])) {
            $path .= '?' . $parsedUrl['query'];
        }

        if (isset($parsedUrl['port'])) {
            $port = $parsedUrl['port'];
        } else {
            // most sites use port 80
            $port = '80';
        }

        $timeout = 10;
        $response = '';
        // connect to the remote server 
        $fp = @fsockopen($host, '80', $errno, $errstr, $timeout);

        if (!$fp) {
            echo "Cannot retrieve $url";
        } else {
            // send the necessary headers to get the file 
            fputs($fp, "GET $path HTTP/1.0\r\n" .
                    "Host: $host\r\n" .
                    "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.3) Gecko/20060426 Firefox/1.5.0.3\r\n" .
                    "Accept: */*\r\n" .
                    "Accept-Language: en-us,en;q=0.5\r\n" .
                    "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n" .
                    "Keep-Alive: 300\r\n" .
                    "Connection: keep-alive\r\n" .
                    "Referer: http://$host\r\n\r\n");

            // retrieve the response from the remote server 
            while ($line = fread($fp, 4096)) {
                $response .= $line;
            }

            fclose($fp);

            // strip the headers
            $pos = strpos($response, "\r\n\r\n");
            $response = substr($response, $pos + 4);
        }

        // return the file content 
        return $response;
    }

    function check() {
        foreach ($this->list as $lnk) {
            $olnk = $lnk;
            $lnk = urldecode($lnk);
            $url5 = "http://validator.w3.org/check?uri=$lnk&amp;ss=1&amp;doctype=HTML5";
            $url = "http://validator.w3.org/check?uri=$lnk&ss=1";
            $bt = microtime(true);
            $start = date('H:i:s');

            $lines = $this->getRemoteFile($url5);
            $f = explode("\n", $lines);

            $elapsetime = sprintf("%-3.2f", (microtime(true) - $bt) / 1);
            print ".";
            flush();
            $fileInfo = new FileInfo($f);
            //pre(); print_r($fileInfo); die();

            $result = "";
            if (is_array($f) && count($f) > 1) {
                foreach ($f as $line) {
                    if (preg_match('@[0-9]+[\t ]+Error@', $line)) {
//if (preg_match('@Validation Output: [0-9]+ Errors@', $line)) {
                        $result .= $line;
                        //print "$line";
                        break;
                    }
                    if (preg_match('@Sorry! This document can not be checked@', $line)) {
                        $result .= "Sorry! This document can not be checked";
                        //print "$line";
                        break;
                    }
                    if (preg_match('@[0-9]+ warning\(s\)@', $line)) {
                        $result .= "$line";
                        //print "$line";
                        break;
                    }
                }
                if ($result === "") {
                    $result = "OK";
                }
            } else {
                $result = "could not open <a href=$url>URL</a> for <a href=$lnk>webpage</a>Check manually";
            }
            //	parent::add("HTML", $olnk, $url, $result, $fileInfo, $start, $elapsetime);
            parent::add("HTML5", $olnk, $url5, $result, $fileInfo, $start, $elapsetime);
        }
        // print "<br>HTML5 inspection completed<br>";
    }

}
?>
}
