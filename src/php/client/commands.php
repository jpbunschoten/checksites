<?php

function commands($debug, $klas) {

    $dn = "";
    $dv = "";
    $tv = "";
    $ln = "";
    $lv = "";
    if ($debug)
        $dv = "checked";
    else
        $dn = "checked";
    $linksonly = true;
    if (isset($_POST['linksonly'])) {
        $linksonly = $_POST['linksonly'] == 'links' ? true : false;
        $tagsonly = $_POST['linksonly'] == 'tags' ? true : false;
        $normalOnly = $_POST['linksonly'] == 'normal' ? true : false;
    }

    if ($linksonly)
        $lv = "checked";
    elseif ($tagsonly)
        $tv = "checked";
    elseif ($normalOnly)
        $ln = "checked";
    else
        $ln = "checked";

    $klassen = new Klassen();

    print '<form method=post action="" name=klasform>';
    $t = new Table();
    foreach ($klassen->get() as $i => $v) {
        $checked = $klas == $v ? " checked " : "";
        $t->radio("k$i", 'klas', $v, $checked, $v)->label("k$i");     
    }
    $t->labels()->td();
  /*
    $t->radio('dt', 'debug', 'true', $dv, 'Debug')->label('dt');
    $t->radio('df', 'debug', 'false', $dn, 'Normal')->label('df');
    $t->labels()->td();
   */ 

    $t->radio('ol', 'linksonly', 'links', $lv, 'only links')->label('ol');
    //$t->radio('ot', 'linksonly', 'tags', $tv, 'only tags')->label('ot');
    $t->radio('nolt', 'linksonly', 'normal', $ln, 'Normal')->label('nolt');
    $t->labels()->td();

    $t->tds()->tr()->trs()->tbody();
    
    print $t->table();
}
