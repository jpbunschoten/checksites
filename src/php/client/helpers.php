<?php

/*
function tx($k,$msg, $in) {
    return "<$k $in>$msg</$k>";
}
function tr($msg, $in = "") {
    return tx("tr",$msg, $in);
}
function ts() {
    return implode(' ', func_get_args());
}
function td($msg, $in = "") {
    return tx("td",$msg, $in);
}
function th($msg, $in = "") {
    return tx("th",$msg, $in);
}
function table($msg, $in = "border=1") {
    return tx("table",$msg, $in);
}
function tbody($msg, $in = '') {
    return tx('tbody', $in);
}
function thead($msg, $in = '') {
    return tx('thead', $in);
}
function label($msg, $id) {
    return tx('label', "$msg", "for='$id' ");
}
function radio($id, $name, $value, $checked, $v = "") {
    if ($v == "") $v = $value;
    return "<input type=radio id=$id name=$name value='$value' $checked>$v";
}
 */
function nl() {
    return "\n";
}


function ttit($s) { return "<tt>$s</tt>\n"; }
function spanit($s, $type="") { return "<span $type>$s</span>\n"; }
function divit($s, $type="") { return "<div $type>\n$s\n</div>\n"; }
function chain() { return implode(' ', func_get_args()); }
function a($href, $extra, $txt) { return "<a href='$href' $extra>$txt</a>"; }
