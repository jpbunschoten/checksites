
<?php

if (!isset($D)) {
    $D = '';
}
print <<<EOI
<script type='text/javascript' charset='utf-8' 
        src='https://code.jquery.com/jquery-2.1.4.min.js'>
</script>

<script type='text/javascript' charset='utf-8'         src='/lib/JQ/jquery.tablesorter.js'></script>
\n
EOI;

foreach (array('Table', 'Html','Debug', 'Site', 'Url','Info','Element', 'Add', 'Extensions',  
    'tableSorter', 'plugins', 'Display', 'Proxy', 'Finder',  'indexLoad') as $f) {
    print "<script type='text/javascript' charset='utf-8'         src='${D}src/js/${f}.js'></script>\n";
}
