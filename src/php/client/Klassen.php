<?php

class Klassen {

    const YR = 15;      // from 2015, etc.
    const DATADIR = "./data/";
    const ME = "jacob.txt";

    public $klassen;

    function __construct() {
        $this->getFromDir();
        return $this;
    }

    private function extension($file, $ext, $pre) {
        $t = preg_match('/' . $pre . '[hv]\.(?i)' . $ext . '$/', $file);
        return $t || $file == self::DATADIR . self::ME;

        return preg_match('/^' . $pre . '[4]?[abchv][^.^:^?^\-]?[^:^?]*\.(?i)' . $ext . '$/', $file);
    }

    /**
     * Get the names of the klassen (like h14 for 2014 havo class
     * @param type $pre
     */
    function getFromDir($pre = self::YR) {
        $dataDirLength = strlen(self::DATADIR);
        $x = array();

        $d = dir(self::DATADIR);
        while (false !== ($entry = $d->read())) {            
            $entry = self::DATADIR . $entry;
            if (is_file($entry) && $this->extension($entry, "txt", $pre)) {
                $x[] = substr($entry, $dataDirLength, strlen($entry) - strlen(".txt") - $dataDirLength);
            }
        }
        $d->close();
        asort($x);
        $this->klassen = $x;
    }
    public function get($pre = self::YR) {
        return $this->klassen;
    }

}
