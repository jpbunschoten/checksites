<?php

class Proxy {

    public $jsonObject;
    public $url;
    public $bron;
    public $user;
    public $index;

    function __construct() {
//$url = filter_input(INPUT_POST, 'url');
        $this->index = filter_input(INPUT_GET, 'index');
        $this->url = filter_input(INPUT_GET, 'url');
        $this->bron = filter_input(INPUT_GET, 'bron');
        $this->user = filter_input(INPUT_GET, 'user');
        return $this;
    }
    function run() {
        $found = false;
        foreach (array('', 'http://', 'http://localhost/', 'http://www.') as $pre) {
            $fileHeaders = @get_headers($pre . $this->url);
            if ($fileHeaders != null || $fileHeaders[0] == 'HTTP/1.1 200 OK') {
                $found = true;
                break;
            }
            //print_r($fileHeaders);
        }
        if ($found == false)
            throw new Exception("url '{$this->url}' could not be opened");

        $f = FILE($pre . $this->url);
        $r = implode(' ', $f);
        $this->jsonObject = json_encode(
                array("result" => true, "index" => $this->index, "bron" => $this->bron,
                    'url' => $this->url, "user" => $this->user, 'data' => $r));
        return $this;
    }

    function toPrint() {
        print $this->jsonObject;
    }

}
