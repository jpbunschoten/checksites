var Debug = {
    debug: false,
    bug: function () {
        this.debug = !this.debug;
        return this;
    },
    bugOn: function () {
        this.debug = true;
        return this;
    },
    bugOff: function () {
        this.debug = false;
        return this
    }
}