/**
 * 
 * manipulatie van URL's, domeinen, users, detectie absolute etc
 */

var Url = {
    uri: '',
    stripHash: function (lnk) {
        var io = lnk.indexOf('#');
        if (io >= 0) {
            //console.log('found # in:', lnk)
            var s = lnk.substr(io)
            lnk = lnk.substr(0, lnk.length - s.length)
        }        
        return lnk
    },
    /**
     *  strip double slashes to single one, except :// pattern
     */
    stripSlashes: function (uri) {
        var s = uri
        s = s.replace(/\/\//g, '/')
        s = s.replace(/:\//, '://')
        s = s.replace(/:\/\/\//, '://')
        return s;
    },
    /**
     * expect *.helenparkhurst.net/bliblablo
     * returns bliblablo    
     */
    stripHome: function (uri) {
        this.uri = uri;
        var s = Site.HP
        var p = this.uri.indexOf(s)
        if (p > 0) {
            return this.uri.substr(p + s.length)
        } else {
            return this.uri;
        }
    },
    stripDotDot: function (uri) {
        var p = 0;
        var t = uri
        while (p !== -1) {
            t = t.replace(/\/[a-zA-Z_-]*\/..\//, '/')
            p = t.indexOf('..');
        }

        return t
    },
    stripBron: function (uri) {
        var s = Add.stripFile(uri);
        s = Url.stripHome(s);
        //console.log('s:' + s + '.')
        if (s === '')
            s = '/'
        //console.log('s:' + s + '.')
        return s;
    }
    ,
    /**
     * true iff uri is relative (i.e home.html or ...)     
     */
    isRelative: function (uri) {
        if (uri.search("^http") != -1)
            return false;
        if (uri.search('^/') != -1)
            return false;
        if (uri.search(Site.home) != -1)
            return false;
        if (uri.search(Site.HP) !== -1)
            return false;
        return true;
    },
    hasHttp: function (uri) {
        if (uri.search("^http") != -1)
            return true;
        return false;
    },
    /**
     *      
     * @returns uri
     */
    toHttp: function (uri) {
        return Url.toAbsolute(uri);
    },
    toSingleHttp: function (uri) {
        var s = uri;
        var p;
        while ((p = s.indexOf('http://http://')) >= 0) {
            s = s.substr("http://".length)
        }
        return s;
    },
    containsSite: function (uri) {
        //var s = uri.substr(uri.indexOf('.')+1)
        var s = uri.indexOf(Site.HP); // >= 0;
        if (Url.debug)
            console.log('containsSite uri:', uri, ",s:", s, 'Site.HP', Site.HP);
        return s !== -1;
    },
    toAbsolute: function (uri) {
        return Url.stripSlashes(Url.stripDotDot(Url.doToAbsolute(uri).replace(/http:..http:../, 'http://')))
    },
    doToAbsolute: function (uri) {
        var http = "http://";

        if (Url.debug)
            console.log('doToAbsolute::' + "uri:'" + uri + "' & home", Site.home)
        if (uri === '')
            return Url.toSingleHttp(http + Site.home + '/');
        if (Url.debug)
            console.log('uri not empty: uri == ' + uri)
        if (Url.startWithSlash(uri)) {
            if (Url.debug)
                console.log('doToAbsolute start with slash: home::', Site.home, "uri::" + uri)
            var s = http + Site.home + uri;
            if (Url.debug)
                console.log("combo of http, home & uri::", s);
            s = Url.toSingleHttp(s);
            if (Url.debug)
                console.log('after toSingleHttp::: ' + s)
            return s;
        }
        if (Url.debug)
            console.log('uri not empty, nor starting with /')
        if (Url.hasHttp(uri)) {
            if (Url.debug)
                console.log('uri has http: returning')
            return uri
        }
        if (Url.isOwner(uri)) {
            if (Url.debug)
                console.log('uri is owned')
            var s = Url.toSingleHttp(http + uri);
            return s;
        }
        if (Url.containsSite(uri)) {
            if (Url.debug)
                console.log('uri contains site')
            return Url.toSingleHttp(http + uri)
        }

        var base = Add.stripFile((Finder.info.bron !== '') ? Finder.info.bron : Site.home);
        if (Url.debug)
            console.log('plain uri home:' + Site.home + ',bron:' + Finder.info.bron + ',base:' + base + ',uri:' + uri)
        var s = http + base + "/" + uri
        if (Url.debug)
            console.log('give to single HTTP:' + s)
        return Url.toSingleHttp(s);
    },
    toHost: function (uri) {
        if (uri.idexOf(Site.HP) != -1)
            return Url.toHttp(Site.home + "/" + uri)
        return uri
    },
    startWithSlash: function (uri) {
        return (uri.search('^/') === 0) ? true : false;
    },
    isAbsolute: function (uri) {
        if (uri.search('^/') == 0)
            return true;
        if (uri.search('^http') == 0)
            return true;
        return false;
    },
    isOwner: function (uri) {
        //console.log(uri)
        //console.log(Finder.home);
        //console.log(uri.indexOf(Finder.home))
        //console.log(Finder.home.indexOf(uri))
        if (uri.indexOf(Site.home) >= 0)
            return true;

        if (Site.home.indexOf(uri) >= 0)
            return true;
        return false;
    },
    isNotOwner: function (uri) {
        return !Url.isOwner(uri)
    },
    getBase: function (uri) {
        var s = uri;
        if (Url.startWithSlash(uri)) {
            s = uri;
        } else if (Url.hasHttp(uri)) {
            s = uri.substr("http://".length);
        }
        var p = s.indexOf('/');
        if (p !== -1) {
            s = s.substr(0, p);
        }
        return s;
    }
};
$.extend(Url, Debug);