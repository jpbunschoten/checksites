var Add = {
    stripFile: function (s) {
        var p = s.lastIndexOf('/');
        var pp = s.indexOf('://');
        if (this.debug)
            console.log('stripFile', s, p, pp)
        if (p === pp + 2)   // so last slash is part of http(s)://
            return s;
        return s.substr(0, p);
    },
    register: function (info) {
        if (this.debug)
            console.log('register', info, ',uri:', info.uri);
        info.run();
        info.index = Finder.src.length
        Finder.src.push(info);
    },
    findTagsAndAttributes: function (info) {
        if (info.content === '')
            return;
        if (this.debug)
            console.log('findTagsAndAttributes', info)
        this.findTags(info);
        if (info.tags.length)   // we found tags in the document
            this.findAttributes(info);
    },
    addStyle: function (info) {
        if ($.inArray("style", info.tagNames) !== -1) {
            info.hasCss = true;
        }
    },
    findTags: function (info) {
        var tgs = [];
        tgs = Element.findTagsInText();

        if (Add.debug)
            console.log('findTags:', tgs);
        
        info.tags = tgs;
        info.tagNames = Element.toTagNames(tgs);
        
        Finder.tags = $.distinct($.merge(Finder.tags, tgs)).sort();
        Finder.tagNames = $.distinct(Element.toTagNames(Finder.tags)).sort();


        this.addStyle(info)

    },
    findAttributes: function (info) {
        var head = []
        $.each(info.tags, function (i, v) {
            if (v.nodeType === 1) {  // meaning .... ELEMENT
                head.push(v);
            }
        });
        head = info.tags
        //console.log('head', head);
        var attrsPrev = Element.allAttributes(head)
        info.attrs = Element.allAttributes(head);
        var attrs = $.distinct($.merge(Element.allAttributes(info.tags), attrsPrev)).sort()
        info.attrs = attrs;
        Finder.attrs = $.distinct($.merge(Finder.attrs, attrs)).sort()
        this.addStyle(info)
    },
    addSite: function () {
        // initial call, site given, parse from here.
        // expect [http[s]://]abc.def.xyz;
        // register user abc, site http[s]://abc.def.xyz as src
        Finder.info = new Info(Site.site, '', Site.home);
        //Finder.info.uri = Site.home;
        Proxy.ajaxCall(0, Site.home, Site.home, Finder.firstTime)
    },
    sameBase: function (val, bron) {
        return (Url.getBase(val) === Url.getBase(bron)) ? true : false;
    },
    add: function (tag) {    // found in uri tag key
        var element = tag.tag
        var key = tag.attr
        var searchKey = tag.searchKey
        var uri = key;
        if (this.debug) {
            console.log('add tag:', tag, ', element:', element, ', searchKey:', searchKey);
            console.log('Finder src:', Finder.src, ', info:', Finder.info)
            console.log('Site :', Site)
        }

        var found = false;

        if (this.debug)
            console.log('Attemp to add uri:', uri, ',key:', key, ',bron:', Finder.info.bron, ',home:', Site.home)

        if (Url.containsSite(uri) && Url.isNotOwner(uri)) {
            if (this.debug)
                console.log('Rejected, containsSite and not Owner')
            return;
        }
        if (Url.hasHttp(uri) && Url.isNotOwner(uri)) {
            if (this.debug)
                console.log('Rejected: has HTTP and not Ownwer')
            return;
        }
        if (Extensions.isNotValid(uri)) {
            if (this.debug)
                console.log('Rejected: extension not valid')
            return;
        }
        //uri = Add.stripFile(Url.toAbsolute(Finder.bron)) + '/' + uri
        var tval = uri; // Url.toAbsolute(uri);
        if (this.debug)
            console.log(Finder.src, tval)
        $.each(Finder.src, function (i, v) {
            var tst1 = v.uri === tval
            var tst2 = v.stripped.bron == Finder.info.strippedBron;
            var tst = tst1; // && tst2
            if (Add.debug)
                console.log('compare:' + i + "::" + v.uri + '& ' + tval + ';' + v.stripped.bron + '&' + Finder.info.strippedBron + '.', tst1, tst2, tst)
            if (tst) {
                found = true;
                return;     // leave this loop 
            }
        });
        if (found) {
            if (this.debug)
                console.log('Rejected: already registered');
            return;
        }
        if (this.debug)
            console.log('Really adding', tval, key, Finder.info.strippedBron)
        var ni = new Info(tval, key, Finder.info.strippedBron)
        ni.content = Finder.info.content;
        ni.run();
        ni.searchKey = searchKey;
        Add.register(ni);
    },
};
$.extend(Add, Debug);