var Finder = {
    data: '',
    bron: '',
    strippedBron: '',
    bronUri: '',
    src: [],
    content: '', // jquery object
    url: '',
    user: '',
    home: '',
    tags: [],
    attrs: [],
    tagNames: [],
    huntLevel: 0,
    hunting: false,
    maxHuntLevel: 50,
    foundTags: '',
    checkFie: function () {
        return true;
    },
    hasToCheck: false,
    reset: function () {
        Finder.data = '';
        Finder.src = [];
        Finder.tags = [];
        Finder.attrs = [];
        Finder.tagNames = [];
        Finder.huntLevel = 0;
        Finder.content = '';
        Finder.url = '';
        Finder.user = '';
        Finder.home = '';
        Finder.foundTags = ''
    },
    isFunction: function (name) {
        return typeof name == 'function';
    },
    prepareToAdd: function (tagName, attrKey, key) {
        var lst = Element.findSrcTagsInText(tagName, attrKey);

        if (this.debug)
            console.log('prepareToAdd (name, attrK, key)', tagName, attrKey, key, lst)
        var t = []
        $.each(lst, function (i, v) {
            t.push({'tag': v.tag, 'id': tagName, 'attr': v.absoluteAttr, 'key': v.absoluteAttr, 'searchKey': key})
        })
        return t;
    },
    testFalse: function () {
        return false;
    },
    testHash: function (element) {
        // return status true is element contains '#'
        var el = element.attr
        //console.log('testHash:el', el)
        var s = Url.stripHash(el);
        //console.log('testHash: s now ', s)
        if (s !== el) {
            if (s.length === 0) {
                return {'element': s, 'status': true, 'type': 'hash'}
            } else {
                return {'element': Finder.Info.uri, 'status': true, 'type': 'hash'}
            }
        }
        return {'element': el, 'status': false, 'type': 'hash'}
    },
    testCss: function (element) {
        //console.log('element', element)
        if (element.rel === 'stylesheet'
                || element.href.indexOf('.css') > 0
                || element.type.indexOf('text/css') >= 0) {
            return {'element': element.href, 'status': true, 'type': 'css'};
        }
        return {'element': '', 'status': false, 'type': 'css'}
    },
    findCss: function () {
        this.checkFie = this.testCss
        var lst = this.prepareToAdd('link', 'href', 'css')
        this.checkFie = this.testFalse;
        return lst;
    },
    findObjects: function () {
        return this.prepareToAdd('object', 'data', 'object')
    },
    findAs: function () {
        this.checkFie = this.testHash
        var lst = this.prepareToAdd('a', 'href', 'a');
        if (this.debug)
            console.log('findAs:', lst)
        this.checkFie = this.testFalse;
        return lst;
    },
    findForm: function () {
        return this.prepareToAdd('form', 'action', 'form')
    },
    findIframe: function () {
        return this.prepareToAdd('iframe', 'src', 'iframe');
    },
    findAllSrcTags: function () {
        var lst = [];

        lst = $.merge(lst, Finder.findAs());
        lst = $.merge(lst, Finder.findForm());
        lst = $.merge(lst, Finder.findIframe());
        lst = $.merge(lst, Finder.findObjects());

        if (this.debug)
            console.log('findAllSrcTags', lst);
        return lst;
    },
    /*
     * It will parse the data and locate new uri (html and css) and add them to the found sources
     * 
     */
    findAndAddSrcTags: function () {
        var lst = this.findAllSrcTags();
        $.each(lst, function (i, v) {
            Add.add(v)
            v.isHTML = true;
        })
        var Csslst = Finder.findCss();
        $.each(Csslst, function (i, v) {
            Add.add(v)
            v.isCSS = true;
        });
    },
    firstTime: function (JsonData) {
        //Finder.info = new Info(Site.site, '', Site.home)
        if (this.debug)
            console.log('FirstTime', this)

        Finder.info.content = JsonData.data
        var ni = new Info(Site.site, '', Site.home);
        //ni.uri = Url.toHttp('/');
        ni.isHTML = true;   // by default
        var x = {'s': JsonData.data}
        var x = $.extend({}, x)
        ni.content = x.s;

        Add.register(ni);   // add the home site
        if (this.debug)
            console.log('registered first time')
        //ni.uri = Site.home
        //console.log('ni', ni)
        Finder.findIt(JsonData);    // start processing, locate SRC tags and process
    },
    findIt: function (JsonData) {
        var indx = JsonData.index;
        this.hunting = false;

        Finder.info.jsonData = JsonData; // save copy                
        Finder.info.uri = JsonData.uri; // data is from this source
        Finder.info.bron = JsonData.bron;    // found in this source
        Finder.info.content = JsonData.data; // content of the url
        Finder.info.strippedBron = Url.stripBron(Finder.info.bron)
        Finder.src[indx].content = JsonData.data
        Finder.src[indx].run();
        Finder.findAndAddSrcTags();
        if (this.debug) {
            console.log('Finder.bron in findIt', Finder.info.bron)
            console.log('Finder content for ' + indx, Finder.src[indx], Finder.info)
        }
        Finder.hunt();
        Display.show();
    },
    hunt: function () {
        $.each(this.src, function (i, v) {
            if (v.searchedTags === false) {
                Add.findTagsAndAttributes(v)
                v.searchedTags = true;
            }
        });
        if (this.hunting)
            return;
        Finder.huntLevel++;
        if (Finder.huntLevel >= Finder.maxHuntLevel)
            return
        var that = this
        $.each(this.src, function (i, v) {
            if (v.hunted === false) {   // bron nog niet geinspecteerd
                v.hunted = true;
                var findIndex = v.uri.indexOf(Site.home);
                if (that.debug)
                    console.log('findIndex hunt:', findIndex, ',bron:' + v.bron, ',uri:' + v.uri, 'v', v)
                if (findIndex !== -1
                        && (findIndex === 0 || findIndex === 7)) {
                    //var d = {klas: v.bron, url: v.uri }
                    // fetch the r.uri (contained in v.bron)
                    Proxy.ajaxCall(i, v.bron, v.uri, Finder.findIt);

                    this.hunting = true;
                    return; // leave $.each loop
                }
            }
        });
    },
}
$.extend(Finder, Debug)