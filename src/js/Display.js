
var Display = {
    reset: function () {
        $('#tags').html('r')
        $('#attrs').html('r')
    },
    keys: ['a', 'object', 'iframe', 'form', 'css', ''],
    lnkChecker: function (info, lnk) {
        var valLnk = "https://validator.w3.org/checklink/docs/checklink?uri=" + lnk + "&hide_type=all&depth=&check=Check"
        return  Html.span(Html.a(valLnk, "id=valLink" + info.uri + " target=_blank", "Links"), "")
    },
    nbsp: function (n) {
        var s = "";
        for (i = 0; i < n; i++) {
            //    s += "&nbsp;"
            s += 'X'
        }

        return s;
    },
    html5Checker: function (info, lnk) {
        var valHTML5 = "http://validator.w3.org/check?uri=" + lnk + "&ss=1";
        //var a = "XXXXX:" + info.nr + ',key:' + info.key;
        var a = this.nbsp(5);
        var valSpans
        // var info = Finder.src[i];
        if (this.debug)
            console.log("html5Checker", info, this.keys)
        //if ($.inArray(info.key, this.keys) !== -1) {
        if (info.isHTML) {
            a = Html.a(valHTML5, "id=valHtml" + info.uri + " target=_blank", "HTML5")
            valSpans = Html.span(a, "")
        } else {
            valSpans = Html.span(a, "style='visibility: hidden;'")
        }

        return valSpans;
    },
    cssChecker: function (info, lnk) {
        var valCSS = $url = "http://jigsaw.w3.org/css-validator/validator?uri=" + lnk;
        //var info = Finder.src[i];
        //console.log(info);
        var a = this.nbsp(3); // "ZZZ";
        var valSpans
        if (info.hasCSS || info.isCSS) {
            a = Html.a(valCSS, "id=valCss" + info.uri + " target=_blank", "CSS")
            valSpans = Html.span(a, "")
        } else {
            valSpans = Html.span(a, "style='visibility: hidden;'")
        }
        return valSpans
    },
    mobileChecker: function (info, lnk) {
        var valM = "https://validator.w3.org/mobile/check?task=201009281822116.mobile2&docAddr=" + lnk
        return  Html.span(Html.a(valM, "id=valMobile" + info.uri + " target=_blank", "MOBILE"), "")
    },
    implode: function (glue, a) {        // could have used join(',')
        var s = "";
        $.each(a, function (i, v) {
            s += glue + v
        })
        return s;
    },
    show: function () {
        $('#information').css({'display': 'inherit'});
        if (this.debug)
            console.log('show src', Finder.src);
        this.reset();
        $.each(Finder.src, function (i, v) {
            if (v.searchedTags === false) {
                Add.findTagsAndAttributes(v)
                v.searchedTags = true;
            }
        })


        var lnk;
        //var lnk = encodeURIComponent(Finder.url);
        $('#Links').html('');
        $('#HTMLS').html('');
        var s = [];
        if (this.debug) {
            console.log("Display", Finder.src);
        }
        $.each(Finder.src, function (i, v) {
            v.calc();
            s.push({'nr': i, 'uri': v.uri, 'key': 1, 'grootte': v.grootte, 'isHTML': v.isHTML, 'isCSS': v.isCSS, 'hasCSS': v.hasCss, 'content': v.content});
        });
        s = $.distinctObjects(s);
        if (this.debug) {
            console.log("found objects", s);
        }
        $.each(s, function (i, v) {
            lnk = v.uri;
            //console.log("lnk", lnk)

            var grootte = '<span style="margin-right:5mm; margin-left:5mm;float:right;">(' + v.grootte + ' karakters)</span>';
            var urlDiv = Html.div(Html.a(lnk, "id=html" + i + " target=_blank", lnk) + grootte, "id=HTML");
            $('#Links').append(urlDiv);

            var spans = [];
            spans.push(Display.html5Checker(v, lnk));
            spans.push(Display.cssChecker(v, lnk));
            spans.push(Display.lnkChecker(v, lnk));
            spans.push(Display.mobileChecker(v, lnk));

            var valDiv = Html.div(spans.join(" | "));

            $('#HTMLS').append(valDiv);
            //this.toolTips();

        })
        Display.showAttributes();
    },
    showAttributes: function () {
        var r = "";
        if (this.debug) {
            console.log('display:tagNames loop', Finder.tagNames)
        }
        $.each(Finder.tagNames, function (i, v) {
            var tagsFromFile = Element.filesFromTagName(v);
            var str = Display.implode("<br>", tagsFromFile);
            r += "<div class=noToolTip>" + v + "</div>";
        });
        $('#tags').html(r);
        r = '';
        $.each(Finder.attrs, function (i, v) {
            r += "<div>" + v + "</div>";
        });
        $('#attrs').html(r);
    },
    toolTips: function () {
        var r = "";
        if (this.debug) {
            console.log('display:tagNames loop', Finder.tagNames)
        }
        $.each(Finder.tagNames, function (i, v) {
            //console.log('display', v)
            var tagsFromFile = Element.filesFromTagName(v);
            //console.log('::', tagsFromFile)
            var str = Display.implode("<br>", tagsFromFile);
            //var tooltip = '<a href="#" title=' + str + ' class=tooltip><span title=' + v + '>' + v + '</span></a>';
            var tooltip = v + ' <span>' + str + '</span>';
            r += "<div class=toolTip>" + tooltip + "</div>";
        });
        $('#tags').html(r);
        r = '';
        $.each(Finder.attrs, function (i, v) {
            r += "<div>" + v + "</div>";
        });
        $('#attrs').html(r);
    }
};
$.extend(Display, Debug)