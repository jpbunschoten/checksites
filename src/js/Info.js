
function Info(uri, key, bron) {    
    this.run = function () {
        //if (this.ran)            return

        this.ran = true;
        this.bron = Url.toAbsolute(uri);   // URL found in this bron
        this.bronUri = Url.stripBron(this.bron);
        this.uri = Url.toAbsolute(uri); // value uri of href, src or ...
        this.stripped = {'bron': Url.stripHome(this.bron), 'url': Url.stripHome(this.uri)}
        if (this.debug) console.log('info ThisObject', this)
        if (!Extensions.isHtml(this.uri))
            this.hunted = true;  // no need to parse
        //if (Extensions.isCss(this.uri)) {
        //    this.hasCss = true;
        //}
        this.calc();
        if (! this.isHTML && Extensions.isHtml(this.uri)) 
            this.isHTML = true
        if (! this.isCSS && Extensions.isCss(this.uri))
            this.isCSS = true
        var hasStyle = this.content.indexOf('<style>') !== -1
        hasStyle = hasStyle || this.content.indexOf('style=') !== -1
        if (! this.hasCss &&  hasStyle)
            this.hasCss = true;
        
    }
    this.calc = function() {
        this.grootte = this.content.length
    }
    this.isCSS = false
    this.isHTML = false;
    this.grootte = -1
    this.index = -1;
    this.ran = false;
    
    //console.log(url, key, bron)
    this.params = {'uri': uri, 'key': key, 'bon': bron}
    this.uri = uri
    this.key = key;                 // a, iframe, ...
    this.bron = bron

    this.content = ''; // Finder.content;      // of this URL
    this.stripped = {}
    this.bronUri = ''


    if (this.debug) {
        console.log('info:', uri, this.uri, 'bron', bron, this.bron, this.stripped)
    }

    this.searchedTags = false;  // URL inspected for tags and attributes?
    this.hunted = false;    // URL examined for more links

    this.tags = [];         // in this URL
    this.tagNames = []      // in this URL
    this.attrNames = [];    // in this URL

    this.hasCss = false;        // URL has CSS (for inspector) 

    this.toStr = function () {
        return "bron:" + this.bron + ", key:" + this.key + ", val:" + this.uri;
    };
};
$.extend(Info, Debug)
