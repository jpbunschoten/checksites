// from http://stackoverflow.com/questions/5381621/jquery-function-to-get-all-unique-elements-from-an-array
// use a = $.distinct([0,1,2,3,3,4] 
// gives [1,2,3,4]
$.extend({
    distinct: function (anArray) {
        var result = [];
        $.each(anArray, function (i, v) {
            if ($.inArray(v, result) === -1)
                result.push(v);
        });
        return result;
    }
});
$.extend({
    containsInArray: function (val, stringArray) {
        //check if val is part of the stringArray elements
        var found = false;
        $.each(stringArray, function (i, str) {
            var p = str.indexOf(val)
            //console.log(val, stringArray, p);
            if (p >= 0) {
                found = true
                return found; // leave loop
            }
        })
        return found;
    }
});

$.extend({
    distinctObjects: function (anArray) {
        //console.log('fkjasdklfjasdkflj', anArray)
        var tmp = []
        var result = [];
        $.each(anArray, function (i, obj) {
            var v = JSON.stringify(obj)
            //console.log('DistingObj:'+ v)
            if ($.inArray(v, tmp) === -1) {
                tmp.push(v);
            }
        });
        $.each(tmp, function (i, objText) {
            result.push(JSON.parse(objText))
        })
        return result;
    }
});

// from http://stackoverflow.com/questions/4376431/javascript-heredoc
// Multiline Function String - Nate Ferrero - Public Domain
$.extend({   
    heredoc: function (f) {
        var s = f.toString().match(/\/\*\s*([\s\S]*?)\s*\*\//m)[1];
        //console.log("heredoc", s);
        return s;
    }
//Use:

//  var txt = heredoc(function(){/*
//  A test of horrible
//  Multi-line strings!
//  */
//  });

});

// http://stackoverflow.com/questions/2048720/get-all-attributes-from-a-html-element-with-javascript-jquery
// var attribs = $('#some_id').getAttributes();
(function ($) {
    $.fn.getAttributes = function () {
        var elem = this,
                attr = {};

        if (elem && elem.length)
            $.each(elem.get(0).attributes, function (v, n) {
                n = n.nodeName || n.name;
                v = elem.attr(n); // relay on $.fn.attr, it makes some filtering and checks
                if (v !== undefined && v !== false)
                    attr[n] = v;
            });

        return attr;
    };
})(jQuery);



// http://stackoverflow.com/questions/1960473/unique-values-in-an-array
// return unique array
/* Array.prototype.unique = function (a) {
 return function () {
 return this.filter(a)
 }
 }(function (a, b, c) {
 return c.indexOf(a, b + 1) < 0
 });
 */
Array.prototype.unique = function () {
    var a = [];
    for (var i = 0, l = this.length; i < l; i++)
        if (a.indexOf(this[i]) === -1)
            a.push(this[i]);
    return a;
};
