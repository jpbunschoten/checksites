
var Element = {
    /**
     * given array of tags [ <a>, <iframe>, ...]  objects, return array of associated names ('a', 'iframe',...
     */
    toTagNames: function (tagList) {
        var tags = [];
        $.each(tagList, function (i, v) {
            var tn = $(v).prop('tagName');
            if (tn !== undefined)
                tags.push(tn.toLowerCase());
        });
        return $.distinct(tags);
    },
    /**
     *  given array of tags, return all attributes found connected to all tags in the list 
     *  typically, all attributes on a page (URL).
     */
    allAttributes: function (tagList) {
        var at = [];
        $.each(tagList, function (i, v) {
            var theTag = $(v).prop('tagName');  // only needed for <IMG>, see below
            var as = [];
            $.each($(v).getAttributes(), function (i2, v2) {
                if (i2 !== undefined) {
                    i2 = i2.toLowerCase();
                    if (theTag === 'IMG') {
                        // skipt width and height only when attached to IMG tag
                        if (!(i2 === 'width' || i2 === 'height')) {
                            as.push(i2);
                        }
                    } else {
                        as.push(i2);
                    }
                }
            });

            at = $.merge(at, as);
        })
        return $.distinct(at);
    }
    ,
    /**
     *  given tagName ('a', 'iframe', or ...) return array of stripped bronnen (van de URL) ...
     */
    filesFromTagNames: function (tagNames) {
        tagNames = $.map(tagNames, function (n, i) {
            return n.toUpperCase();
        });
        var s = [];
        $.each(Finder.src, function (i, v) {  // v:: Info
            // loop on all found files, get the accumulated tags
            $.each(Finder.src[i].tags, function (j, t) {
                if (this.debug)
                    console.log('tagsFromTag', v, t, t.name, tagNames)
                if ($.inArray(t.nodeName, tagName) !== -1) {
                    s.push(v.stripped.bron);
                }
            })
        })
        return s.unique();
    },
    filesFromTagName: function (tagName) {
        tagName = tagName.toLowerCase();

        var s = [];
        $.each(Finder.src, function (i, v) {  // v:: Info
            // loop on all found files, get the accumulated tags
            $.each(v.tagNames, function (j, t) {
                if (this.debug)
                    console.log('tagsFromTag', v, v.uri, t, tagName)
                //if ($.inArray(t, tagName) !== -1) {
                if (t === tagName) {
                    s.push(Url.stripHome(v.uri));
                }
            })
        })
        return s.unique();
    }
    ,
    filesFromSrcTag: function (tagName) {
        var s = [];
        //console.log(tag, this.src, this.tags, this.attrs)
        $.each(Finder.src, function (i, v) {
            if (this.debug)
                console.log('filesFromSrcTag', v);
            if (v.key === tagName) {
                s.push(v.stripped.bron);
            }
        })
        s = s.unique();

        return s;
    },
    findSrcTagsInText: function (tagName, attrKey) {
        var uTag = tagName.toUpperCase();
        var lst = [];
        var that = this;
        var a1 = $(Finder.info.content);
        var a2 = $(Finder.info.content).find(tagName);
        if (this.debug) {
            console.log("content $()", a1)
            console.log("content $(),find(...)", a2)
        }
        var nodes = $.merge(a1, a2)
        $.each(nodes, function (i, v) {
            if (that.debug)
                console.log("fndSrcTagsInText", i, v)
            try {
                if (v.nodeName === uTag) {
                    var absoluteAttr = $(v).attr(attrKey)
                    absoluteAttr = Url.toAbsolute(absoluteAttr);
                    if (that.debug)
                        console.log('findSrcTagsInText ...:absoluteAttr', absoluteAttr)
                    var p = absoluteAttr.indexOf(Site.href)
                    if (p >= 0) {
                        absoluteAttr = absoluteAttr.substr(Site.href.length);
                        if (that.debug)
                            console.log('findSrcTagsInText::p:', p, ',el:', absoluteAttr)
                    }
                    if (that.debug)
                        console.log('findSrcTagsInText testHash', v, absoluteAttr)
                    if (absoluteAttr !== undefined) {
                        v.attr = absoluteAttr;
                        if (Finder.checkFie !== Finder.testFalse) {                            
                            var tst = Finder.checkFie(v);   // True or False
                            //console.log('JBN', v, tst)
                            if (tst.status) {
                                v.attr = tst.element
                                if (tst.type === 'css')
                                    lst.push({'tag': v, 'attr': tst.element, 'absoluteAttr': absoluteAttr})
                                if (tst.type === 'hash') {
                               //     console.log('JBN', v, tst);
                                    lst.push({'tag': v, 'attr': tst.element, 'absoluteAttr': tst.element})
                                }
                                //lst.push({'tag': v, 'attr': absoluteAttr, 'absoluteAttr': absoluteAttr})
                            } else {
                                lst.push({'tag': v, 'attr': absoluteAttr, 'absoluteAttr': absoluteAttr})
                            }
                        } else {
                            lst.push({'tag': v, 'attr': absoluteAttr, 'absoluteAttr': absoluteAttr})
                        }
                    }
                }
            } catch (e) {
                if (that.debug)
                    console.log('exception on', i, v)
            }
        })

        return lst;
    },
    findTagsInText: function () {
        var lst = [];
        var t = Finder.info.content;
        var doc = (new DOMParser()).parseFromString(t, "text/html").all;
        //var doc = $(t).find('*');
        //doc = $.extend(doc, $(t).find('*'))       

        $.each(doc, function (i, v) {
            try {
                if (v.nodeType === 1) {
                    lst.push(v);
                }
            } catch (e) {
            }
        });
        return lst;
    }
};
$.extend(Element, Debug);