var Extensions = {
    valid: [".html", , ".htm", '.css'],
    html: ['.html', '.htm', ''],
    forbidden: ['.odt', '.mp3', '.php', '.asp', '.ico','.png', '.gif', '.lgi'],
    forbiddenViaIndexOf: ['mailto:', '.ico'],
    getExtension: function (ex) {
        var p;

        p = ex.lastIndexOf('/') + 1     // find slash in abc/
        //console.log(ex, p, ex.length)
        if (p === ex.length) {      // it is the last slash (so dir, so vallid)
            return true;
        }
        p = ex.lastIndexOf('.');    // find dot in .html in abc.html
        if (p === -1) {
            return true;    // no dot, so valid extension
        }
        var s = ex.substr(p)
        return s;
    },
    isValid: function (ex) {
        var s = this.getExtension(ex);

        var allowedOnes = $.inArray(s, Extensions.valid) !== -1; // found, so valid
        var forbiddenOnes = $.inArray(s, Extensions.forbidden) === -1; // not found, so not valid
        //   console.log(ex, s, allowedOnes, forbiddenOnes);
        var allow = true
        $.each(this.forbiddenViaIndexOf, function(i, v) {
            if (ex.indexOf(v) >= 0) { allow = false; return }
        })
        return  forbiddenOnes && allow;
    },
    isNotValid: function (ex) {
        return !Extensions.isValid(ex);
    },
    isHtml: function (ex) {
        var s = this.getExtension(ex)
        return $.inArray(s, Extensions.html) !== -1 
    },
    isCss: function (ex) {
        var s = this.getExtension(ex)
        return s == ".css"
    }
};