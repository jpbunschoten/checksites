var Table = {
    tSorterId: "myTable",
    tClass: 'tablesorter',
    _thead: '',
    _tbody: '',
    _ths: [],
    _tds: [],
    _trs: [],
    run: function () {
        if (this._thead === '') {

            this._thead = this.thead(ths);
        } else if (this._tbody === '') {
            var trs = join('', this._trs);
            this._trs = []
            this._thead = this.tbody(trs);
        }
    },
    t: function (tag) {
        return "<" + tag + ">"
    },
    p: function (tag, s) {
        return this.t(tag) + s + this.t("/" + tag)
    },
    table: function () {
        var s = this.t('<table' + ' class="' + this.tClass + '" id=' + this.tSorterId + '>')
        s += _thead
        s += _tbody
        s += this.t('/table')
        return s;
    },
    thead: function (s) {
        var ths = join('', this._ths);
        this._ths = []       
        this.tr(ths);        
        this._thead = this.p('thead', join('', this._trs))
        this._trs = [];
        return this;
    },
    tbody: function (s) {
        var trs = join('', this._trs);
        this._trs = []       
        this._thead = this.p('tbody', join('', trs))
        return this;
        
    },
    tr: function (s) {
        this._trs.push(this.p('tr', s))
        return this;
    },
    th: function (s) {
        this._ths.push(this.p('th', s))
        return this;
    },
    td: function (s) {
        this._tds.push(this.p('td', s))
        return this
    },
    ts: function () {
        var s = ""
        for (var i = 0; i < arguments.length; i++)
            s += arguments[i]
        return s;
    }
}