var Proxy = {
    async : true,
    href: 'http://localhost/checkSites/src/php/server/runProxy.php', // to proxy
   
    sync: function() {
      this.async = false;  
      return this;
    },
    onError: function (data) {
        //if (this.debug)
            console.log("proxy onError", data)
            Finder.hunting = true;  // allow for further inspection
    },
    onComplete: function (data) {
        if (this.debug)
            console.log("proxy onComplete", data)
    },
    ajaxCall: function (index, bron, urlToLookAt, onSuccess) {
        var that = this
        var ajaxUrl = this.hrefUrl(index, bron, urlToLookAt);
        if (this.debug) {
            console.log('proxy.js: ajaxCall', bron, urlToLookAt, ' URL ', ajaxUrl)
        }
        $.ajax({
            async : Proxy.async,
            url: ajaxUrl,
            method: 'GET',
            dataType: 'JSON',
            data: '',
            success: onSuccess,
            error: that.onError,
            complete: that.onComplete
        });
    },
    hrefUrl: function (index,  bron, urlToLookAt) {
        return this.href + "?" + $.param({'index': index, 'user': Site.user, 'bron': bron, 'url': urlToLookAt});
    }
};
$.extend(Proxy, Debug);
