var Html = {
    a: function (href, extra, txt) {
        return "<a href='" + href + "'" + extra + ">" + txt + "</a>";
    },
    div: function (msg, xtra) {
        return "<div " + xtra + " >" + msg + "</div>";
    },
    span: function (msg, xtra) {
        return "<span " + xtra + " >" + msg + "</span>";
    }
};