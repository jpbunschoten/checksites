$(document).ready(function (){
    $.tablesorter.defaults.widgets = ['zebra'];
    $("#myTable").tablesorter({
        // sort on the first column and second column, order asc 
        sortList: [[1, 0], [0, 0]]                
    });
});

