<?php
$Ipath = get_include_path();
set_include_path(implode(PATH_SEPARATOR, array($Ipath, "src/php/client", "src/php/server") ));
include_once 'helpers.php';

function autoLoadBaseClassName($base, $className) {
    if (strpos($className, "PHP_") === 0)
        return;
    if (strpos($className, "PHPUnit_") === 0)
        return;
    if (strpos($className, "Composer") === 0)
        return;
    if (strpos($className, "m") === 0 & strlen($className) == 1)
        return; // Mockery

        
//print "className:$className";    $a = strpos("PHPUnit_", $className);    var_dump($a);

    if (strpos($className, "test") !== false) {
        print $className;
        $className = substr($className, strpos("test", $className));
        print $className;
        //    include_once $className . ".php";
        die();
    }
    $filename = __DIR__ . "/$base/" . $className . ".php";
    $filename = $className . ".php";
    // print $filename; print "\n";
    //if (is_readable($filename)) {
        //print $filename; print "\n";
        require_once $filename;
    //}
}

function autoLoadSubSystems($className) {
    // should be dynamically
    foreach (array('', 'src') as $d) {
        autoLoadBaseClassName($d, $className);
    }
}


spl_autoload_register('autoLoadSubSystems');

